package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.model.ReportResiduesMaterial
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife


class ReportMaterialResiduesAdapter(private var materialsResidues: List<ReportResiduesMaterial>) : RecyclerView.Adapter<ReportMaterialResiduesAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: ReportResiduesMaterial = materialsResidues[position]
        holder.materialResiduesName.text = item.name
        holder.materialResiduesCount.text = item.value.toString()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.report_material_residues_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = materialsResidues.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.report_material_residues_name)
        lateinit var materialResiduesName: TextView
        @BindView(R.id.report_material_residues_text)
        lateinit var materialResiduesCount: TextView

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}