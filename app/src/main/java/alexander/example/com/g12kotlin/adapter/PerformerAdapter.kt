package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.model.Performer
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife


class PerformerAdapter(private var performerList: List<Performer>) : RecyclerView.Adapter<PerformerAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: PerformerAdapter.ViewHolder, position: Int) {
        holder.fullName.text = performerList[position].firstName + " " + performerList[position].lastname
    }

    override fun getItemCount(): Int =
            performerList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PerformerAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.performer_layout, parent, false)
        return PerformerAdapter.ViewHolder(view)
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.performer_full_name)
        lateinit var fullName: TextView

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}