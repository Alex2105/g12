package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.api.SingletonRest
import alexander.example.com.g12kotlin.errors.Error
import alexander.example.com.g12kotlin.model.OrderKindWork
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SwitchCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class OrderKindWorkAdapter(var workList: List<OrderKindWork>, var context: Context, var token: String) : RecyclerView.Adapter<OrderKindWorkAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.kind_of_work_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.taskName.text = workList[position].name
        holder.switchTask.isChecked = workList[position].created
        holder.switchTask.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, isChecked ->
            if (isChecked) {
                SingletonRest.sendSubtask(token, workList[position].id, true)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ subtaskModel ->
                            if (subtaskModel.success) {
                                holder.switchTask.isChecked = true
                                workList[position].created = isChecked
                            } else {
                                Toast.makeText(context, "Задача уже выполнена", Toast.LENGTH_SHORT).show()
                            }
                        }, { throwable -> error.handleError(throwable) })
            } else {
                holder.switchTask.isChecked = true
            }
        })
    }

    fun workList(workList: List<OrderKindWork>) {
        this.workList = workList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = workList.size

    private var error: Error = Error(context)


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.kind_work_task_name)
        lateinit var taskName: TextView
        @BindView(R.id.kind_work_switch)
        lateinit var switchTask: SwitchCompat

        init {
            ButterKnife.bind(this, itemView)
        }
    }

}