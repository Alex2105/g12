package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 29.01.18.
 */
class AccountData(
        @SerializedName("id") val id: Int,
        @SerializedName("firstname") val firstName: String,
        @SerializedName("phone") val phone: String,
        @SerializedName("lastname") val lastname: String,
        @SerializedName("email") val email: String?
)