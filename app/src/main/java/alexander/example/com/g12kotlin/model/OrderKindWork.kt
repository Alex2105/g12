package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 01.02.18.
 */
class OrderKindWork(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("created?") var created: Boolean
)