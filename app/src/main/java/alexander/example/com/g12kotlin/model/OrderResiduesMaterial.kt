package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 02.02.18.
 */
class OrderResiduesMaterial(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("value") val value: Int,
        @SerializedName("spent_value") var userValue: Int,
        var flag: Boolean
)