package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 30.01.18.
 */
class SendTaskMaterial(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("value") val value: String,
        @SerializedName("usr_present?") val userIdPresent: Boolean)