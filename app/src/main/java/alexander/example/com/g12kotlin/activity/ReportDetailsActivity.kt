package alexander.example.com.g12kotlin.activity

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.R.string.report_details
import alexander.example.com.g12kotlin.adapter.ReportCommentAdapter
import alexander.example.com.g12kotlin.adapter.ReportKindWorkAdapter
import alexander.example.com.g12kotlin.adapter.ReportMaterialAdapter
import alexander.example.com.g12kotlin.adapter.ReportMaterialResiduesAdapter
import alexander.example.com.g12kotlin.api.SingletonRest
import alexander.example.com.g12kotlin.calendar.CalendarData
import alexander.example.com.g12kotlin.errors.Error
import alexander.example.com.g12kotlin.interfaces.KEY_REPORT_ID
import alexander.example.com.g12kotlin.interfaces.TOKEN
import alexander.example.com.g12kotlin.model.*
import alexander.example.com.g12kotlin.save.Preferences
import alexander.example.com.g12kotlin.utils.MaterialProgressDialog
import android.app.Activity
import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.widget.ImageButton
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class ReportDetailsActivity : Activity() {
    @BindView(R.id.task_report_date)
    lateinit var reportDate: TextView
    @BindView(R.id.task_report_name)
    lateinit var reportName: TextView
    @BindView(R.id.task_report_description)
    lateinit var reportDescription: TextView
    @BindView(R.id.arrow_back)
    lateinit var arrow: ImageButton
    @BindView(R.id.title)
    lateinit var title: TextView
    @BindView(R.id.report_kind_text)
    lateinit var kindText: TextView
    @BindView(R.id.report_material_text)
    lateinit var materialText: TextView
    @BindView(R.id.report_kilometers_text)
    lateinit var kilometrsText: TextView
    @BindView(R.id.report_kilometer_text)
    lateinit var distanceText: TextView
    @BindView(R.id.report_distance)
    lateinit var distance: TextView
    @BindView(R.id.report_comment_text)
    lateinit var commentText: TextView
    @BindView(R.id.report_comment)
    lateinit var comment: TextView
    @BindView(R.id.driver_name)
    lateinit var driverName: TextView
    @BindView(R.id.report_material_residues_text)
    lateinit var materialResiduesText: TextView
    @BindView(R.id.report_kind_work_recycler_view)
    lateinit var kindWorkRecyclerView: RecyclerView
    @BindView(R.id.report_material_recycler_view)
    lateinit var materialRecyclerView: RecyclerView
    @BindView(R.id.report_material_residues_recycler_view)
    lateinit var materialResiduesRecyclerView: RecyclerView
    @BindView(R.id.report_material_comment_recycler_view)
    lateinit var reportCommentRecyclerView: RecyclerView
    private lateinit var materialProgressDialog: MaterialProgressDialog
    private var reportId: Int = -1
    private lateinit var error: Error

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_details)
        ButterKnife.bind(this)
        val context = this
        error = Error(context)
        materialProgressDialog = MaterialProgressDialog.getProgressDialog(this@ReportDetailsActivity)
        val preferences = Preferences(context)
        val token = preferences.loadToken(TOKEN)
        val intent: Intent? = intent
        if (intent != null) reportId = intent.getIntExtra(KEY_REPORT_ID, -1)
        getReportTask(token, reportId)
        initActionBar()
    }

    @OnClick(R.id.arrow_back)
    internal fun arrowClick() = this.onBackPressed()

    private fun initActionBar() {
        title.text = getString(report_details)
    }

    private fun initCommentsRecyclerView(comments: List<Comment>) {
        reportCommentRecyclerView.setHasFixedSize(true)
        reportCommentRecyclerView.layoutManager = initLayoutManager()
        val reportCommentAdapter = ReportCommentAdapter(comments)
        reportCommentRecyclerView.adapter = reportCommentAdapter
    }

    private fun getReportTask(token: String, reportId: Int) {
        if (reportId != -1) {
            materialProgressDialog.show()
            SingletonRest.getReportsInformations(token, reportId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::setReportData, { throwable -> error.handleError(throwable) })

        }
    }

    private fun setReportData(reportTask: ReportTask) {
        initKindWorkRecyclerView(reportTask.subtasks)
        if (reportTask.materials.isEmpty()) {
            materialText.visibility = View.GONE
        }
        if (reportTask.reportResiduesMaterials.isEmpty()) {
            materialResiduesText.visibility = View.GONE
        }
        if (reportTask.comments.isEmpty()) {
            commentText.visibility = View.GONE
        }
        initMaterialRecyclerView(reportTask.materials)
        initCommentsRecyclerView(reportTask.comments)
        initMaterialResiduesRecyclerView(reportTask.reportResiduesMaterials)
        if (reportTask.address.isEmpty()) {
            reportName.text = reportTask.title
        } else {
            reportName.text = reportTask.title + ", " + reportTask.address
        }
        driverName.text = reportTask.driverFullName
        distance.text = reportTask.gasoline.value.toString()
        reportDescription.text = reportTask.description
        try {
            reportDate.text = CalendarData.dateFormatFromServer(reportTask.date)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        materialProgressDialog.dismiss()

    }

    private fun initKindWorkRecyclerView(reportKindWorks: List<ReportKindWork>) {
        kindWorkRecyclerView.setHasFixedSize(true)
        kindWorkRecyclerView.layoutManager = initLayoutManager()
        val reportKindWorkAdapter = ReportKindWorkAdapter(reportKindWorks)
        kindWorkRecyclerView.adapter = reportKindWorkAdapter
    }

    private fun initLayoutManager(): LinearLayoutManager = object : LinearLayoutManager(this) {
        override fun canScrollVertically(): Boolean = false
    }

    private fun initMaterialRecyclerView(reportMaterialList: List<ReportMaterial>) {
        materialRecyclerView.setHasFixedSize(true)
        materialRecyclerView.layoutManager = initLayoutManager()
        val reportMaterialAdapter = ReportMaterialAdapter(reportMaterialList)
        materialRecyclerView.adapter = reportMaterialAdapter
    }

    private fun initMaterialResiduesRecyclerView(reportResiduesMaterials: List<ReportResiduesMaterial>) {
        materialRecyclerView.setHasFixedSize(true)
        materialRecyclerView.layoutManager = initLayoutManager()
        val reportMaterialResiduesAdapter = ReportMaterialResiduesAdapter(reportResiduesMaterials)
        materialRecyclerView.adapter = reportMaterialResiduesAdapter
    }

}