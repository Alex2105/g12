package alexander.example.com.g12kotlin.api

import alexander.example.com.g12kotlin.model.*
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


object SingletonRest {
    private val TEST_URL: String = "https://g12crm.herokuapp.com/api/"
    private val iAuthorization: ServerRequest.IAuthorization
    private val iLogOut: ServerRequest.ILogOut
    private val iAccountData: ServerRequest.IAccountData
    private val iTaskMaterial: ServerRequest.ITaskMaterial
    private val iUpdateTaskMaterials: ServerRequest.IUpdateTaskMaterials
    private val iAddMaterial: ServerRequest.IAddMaterial
    private val iReportTask: ServerRequest.IReportTask
    private val iSubtask: ServerRequest.ISubtask
    private val iTasks: ServerRequest.ITasks
    private val iTaskData: ServerRequest.ITaskData

    init {
        val logging = HttpLoggingInterceptor().
                setLevel(HttpLoggingInterceptor.Level.HEADERS)
        val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()
        val retrofit = Retrofit.Builder()
                .baseUrl(TEST_URL)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        iAuthorization = retrofit.create(ServerRequest.IAuthorization::class.java)
        iLogOut = retrofit.create(ServerRequest.ILogOut::class.java)
        iAccountData = retrofit.create(ServerRequest.IAccountData::class.java)
        iTaskMaterial = retrofit.create(ServerRequest.ITaskMaterial::class.java)
        iUpdateTaskMaterials = retrofit.create(ServerRequest.IUpdateTaskMaterials::class.java)
        iAddMaterial = retrofit.create(ServerRequest.IAddMaterial::class.java)
        iReportTask = retrofit.create(ServerRequest.IReportTask::class.java)
        iSubtask = retrofit.create(ServerRequest.ISubtask::class.java)
        iTasks = retrofit.create(ServerRequest.ITasks::class.java)
        iTaskData = retrofit.create(ServerRequest.ITaskData::class.java)
    }

    fun authorization(phoneNumber: String, password: String): Observable<Authorization> =
            iAuthorization.setAuthorization(phoneNumber, password)

    fun logOut(token: String): Observable<AccountLogOut> =
            iLogOut.setLogOut(token)

    fun getAccountData(token: String, id: Int): Observable<AccountData> =
            iAccountData.setAccountData(token, id)

    fun setMaterials(token: String, materialTask: MaterialTask): Observable<SubtaskMaterial> =
            iTaskMaterial.setMaterials(token, materialTask)

    fun getUpdateMaterials(token: String, taskId: Int): Observable<Materials> =
            iUpdateTaskMaterials.getMaterialsList(token, taskId)

    fun setMaterial(token: String, taskMaterials: TaskMaterials): Observable<TaskAddMaterial> =
            iAddMaterial.setMaterial(token, taskMaterials)

    fun getReportsInformations(token: String, id: Int): Observable<ReportTask> =
            iReportTask.getReportsData(token, id)

    fun sendSubtask(token: String, subtaskId: Int, subtaskState: Boolean): Observable<SubtaskModel> =
            iSubtask.sendSubtask(token, subtaskId, subtaskState)

    fun tasks(token: String, data: String): Observable<Tasks> =
            iTasks.getTasks(token, data)

    fun getOrdersInformations(token: String, id: Int): Observable<OrderTask> =
            iTaskData.getOrdersData(token, id)


}