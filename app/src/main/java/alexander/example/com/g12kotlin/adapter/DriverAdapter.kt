package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.model.Driver
import android.content.DialogInterface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife


class DriverAdapter(var driverList: List<Driver>) : RecyclerView.Adapter<DriverAdapter.ViewHolder>() {
    var selectedPosition = -1
    private var userId = 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fullName.text = driverList[position].firstname + " " + driverList[position].lastname
        if (selectedPosition == position) {
            holder.imageView.setImageResource(R.drawable.completed_task)
            userId = driverList[position].id
        } else {
            holder.imageView.setImageResource(0)
        }
    }

    override fun getItemCount(): Int = driverList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.driver_item, parent, false)
        return ViewHolder(view)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        @BindView(R.id.driver_full_name)
        lateinit var fullName: TextView
        @BindView(R.id.driver_image)
        lateinit var imageView: ImageView

        init {
            ButterKnife.bind(this, itemView)
        }

        override fun onClick(p0: View?) {
            if (adapterPosition == RecyclerView.NO_POSITION) return

            notifyItemChanged(selectedPosition)
            selectedPosition = adapterPosition
            notifyItemChanged(selectedPosition)
        }

    }
}