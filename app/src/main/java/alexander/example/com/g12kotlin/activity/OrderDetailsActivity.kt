package alexander.example.com.g12kotlin.activity

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.adapter.*
import alexander.example.com.g12kotlin.api.SingletonRest
import alexander.example.com.g12kotlin.calendar.CalendarData
import alexander.example.com.g12kotlin.save.Preferences
import alexander.example.com.g12kotlin.utils.MaterialProgressDialog
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import alexander.example.com.g12kotlin.errors.Error
import alexander.example.com.g12kotlin.interfaces.KEY_TASK_ID
import alexander.example.com.g12kotlin.interfaces.TOKEN
import alexander.example.com.g12kotlin.interfaces.USER_ID
import alexander.example.com.g12kotlin.model.*
import android.opengl.Visibility
import android.support.v7.widget.LinearLayoutManager
import butterknife.OnClick
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by alexander on 01.02.18.
 */
class OrderDetailsActivity : Activity() {
    @BindView(R.id.order_task_name)
    lateinit var name: TextView
    @BindView(R.id.order_task_description)
    lateinit var description: TextView
    @BindView(R.id.title)
    lateinit var title: TextView
    @BindView(R.id.arrow_back)
    lateinit var arrow: ImageButton
    @BindView(R.id.order_send_report)
    lateinit var sendReport: Button
    @BindView(R.id.project_name)
    lateinit var projectName: TextView
    @BindView(R.id.project_text)
    lateinit var projectText: TextView
    @BindView(R.id.order_zero_view)
    lateinit var line: View
    @BindView(R.id.order_task_date)
    lateinit var textDate: TextView
    @BindView(R.id.order_description_text)
    lateinit var descriptionText: TextView
    @BindView(R.id.order_kind_text)
    lateinit var kindText: TextView
    @BindView(R.id.order_add_new_task)
    lateinit var addTask: TextView
    @BindView(R.id.order_material_text)
    lateinit var materialText: TextView
    @BindView(R.id.order_kilometers_text)
    lateinit var distanceText: TextView
    @BindView(R.id.order_kilometer_text)
    lateinit var kilometerText: TextView
    @BindView(R.id.order_distance_edit_text)
    lateinit var distance: EditText
    @BindView(R.id.order_comment_item_text)
    lateinit var commentText: TextView
    @BindView(R.id.order_comment)
    lateinit var commentEditText: EditText
    @BindView(R.id.order_material_residues_text)
    lateinit var materialResiduesText: TextView
    @BindView(R.id.order_add_comment)
    lateinit var addComment: ImageButton
    @BindView(R.id.order_kind_work_recycler_view)
    lateinit var kindWorkRecyclerView: RecyclerView
    @BindView(R.id.order_material_recycler_view)
    lateinit var materialRecyclerView: RecyclerView
    @BindView(R.id.order_material_residues_recycler_view)
    lateinit var materialResiduesRecyclerView: RecyclerView
    @BindView(R.id.order_material_comment_recycler_view)
    lateinit var orderCommentRecyclerView: RecyclerView
    @BindView(R.id.performer_recycler_view)
    lateinit var performentRecyclerView: RecyclerView
    @BindView(R.id.drivers_recycler_view)
    lateinit var driversRecyclerView: RecyclerView
    @BindView(R.id.swipeRefreshLayout)
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    @BindView(R.id.fourth_order_view)
    lateinit var fourthView: View
    private lateinit var token: String
    private lateinit var currency: String
    var iTaskId = -1
    private lateinit var preferences: Preferences
    private lateinit var context: Context
    private lateinit var subtasksList: List<OrderKindWork>
    private lateinit var materialProgressDialog: MaterialProgressDialog
    private lateinit var orderCommentAdapter: OrderCommentAdapter
    private lateinit var kindWorkAdapter: OrderKindWorkAdapter
    private lateinit var orderMaterialAdapter: OrderMaterialAdapter
    private lateinit var orderMaterialResiduesAdapter: OrderMaterialResiduesAdapter
    private lateinit var driverAdapter: DriverAdapter
    private lateinit var performerAdapter: PerformerAdapter
    private lateinit var error: Error


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_details)
        ButterKnife.bind(this)
        context = this
        error = Error(context)
        subtasksList = ArrayList()
        materialProgressDialog = MaterialProgressDialog.getProgressDialog(this@OrderDetailsActivity)
        preferences = Preferences(context)
        token = preferences.loadToken(TOKEN)
        val intent = intent
        if (intent != null) {
            iTaskId = intent.getIntExtra(KEY_TASK_ID, -1)
        }
        getTasks(token, taskId, true)
        swipe()
        initActionBar()
    }

    private fun brigadier(ordersData: OrderTask) {
        val brigadierId = preferences.loadUserId(USER_ID)
        if (brigadierId == ordersData.brigadierId) {
            sendReport.visibility = View.VISIBLE
            distanceText.visibility = View.VISIBLE
            distance.visibility = View.VISIBLE
            driversRecyclerView.visibility = View.VISIBLE
            fourthView.visibility = View.VISIBLE
            kilometerText.visibility = View.VISIBLE
            initDriverRecyclerView(ordersData.getDriverList())
        } else {
            distanceText.visibility = View.GONE
            driversRecyclerView.visibility = View.GONE
            sendReport.visibility = View.GONE
            fourthView.visibility = View.GONE
            kilometerText.visibility = View.GONE
            distance.visibility = View.GONE
        }
    }

    private fun swipe() {
        swipeRefreshLayout.setColorSchemeColors(R.color.blue)
        swipeRefreshLayout.setOnRefreshListener {
            getTasks(token, taskId, false)
        }
    }

    private fun getTasks(token: String, taskId: Int, flag: Boolean) {
        if (taskId != -1) {
            if (flag) {
                materialProgressDialog.show()
            }
            SingletonRest.getOrdersInformations(token, taskId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::setOrdersData, { throwable -> error.handleError(throwable) })
        }
    }

    private fun setOrdersData(ordersData: OrderTask) {
        if (ordersData.materials.isEmpty()) {
            materialText.visibility = View.GONE
        }
        if (ordersData.usersMaterials.isEmpty()) {
            materialResiduesText.visibility = View.GONE
        }
        if (ordersData.projectName.isNotEmpty()) {
            projectName.visibility = View.VISIBLE
            projectText.visibility = View.VISIBLE
            line.visibility = View.VISIBLE
            projectName.text = ordersData.projectName
        } else {
            projectName.visibility = View.GONE
            projectText.visibility = View.GONE
            line.visibility = View.GONE
        }
        brigadier(ordersData)

        initKindWorkRecyclerView(ordersData.subtasks)
        initPerformerRecyclerView(ordersData.performerList)
        initMaterialRecyclerView(ordersData.materials)
        initMaterialResiduesRecyclerView(ordersData.usersMaterials)
        initCommentsRecyclerView(ordersData.comments)
        name.text = ordersData.title + ", " + ordersData.address
        description.text = ordersData.description

        try {
            textDate.text = CalendarData.dateFormatFromServer(ordersData.date)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        materialProgressDialog.dismiss()
        swipeRefreshLayout.isRefreshing = false
    }

    private fun initKindWorkRecyclerView(worksList: List<OrderKindWork>) {
        kindWorkRecyclerView.setHasFixedSize(true)
        kindWorkRecyclerView.layoutManager = initLayoutManager()
        kindWorkAdapter = OrderKindWorkAdapter(worksList, context, token)
        kindWorkRecyclerView.adapter = kindWorkAdapter
    }

    private fun initDriverRecyclerView(driverList: List<Driver>) {
        driversRecyclerView.setHasFixedSize(true)
        driversRecyclerView.layoutManager = initLayoutManager()
        driverAdapter = DriverAdapter(driverList)
        driversRecyclerView.adapter = driverAdapter
    }

    private fun initPerformerRecyclerView(performerList: List<Performer>) {
        performentRecyclerView.setHasFixedSize(true)
        performentRecyclerView.layoutManager = initLayoutManager()
        performerAdapter = PerformerAdapter(performerList)
        performentRecyclerView.adapter = performerAdapter
    }

    private fun initCommentsRecyclerView(comments: List<Comment>) {
        orderCommentRecyclerView.setHasFixedSize(true)
        orderCommentRecyclerView.layoutManager = initLayoutManager()
        orderCommentAdapter = OrderCommentAdapter(comments)
        orderCommentRecyclerView.adapter = orderCommentAdapter
    }

    private fun initLayoutManager(): LinearLayoutManager = object : LinearLayoutManager(this) {
        override fun canScrollVertically(): Boolean = false
    }

    private fun initMaterialRecyclerView(materialsList: List<OrderMaterial>) {
        materialRecyclerView.setHasFixedSize(true)
        materialRecyclerView.layoutManager = initLayoutManager()
        for (i in 0 until materialsList.size) {
            materialsList[i].maxValue = materialsList[i].value
            materialsList[i].flag = true
        }
        orderMaterialAdapter = OrderMaterialAdapter(materialsList)
        materialRecyclerView.adapter = orderMaterialAdapter
    }

    private fun initMaterialResiduesRecyclerView(materialResiduesTasks: List<OrderResiduesMaterial>) {
        materialResiduesRecyclerView.setHasFixedSize(true)
        for (i in 0 until materialResiduesTasks.size) {
            materialResiduesTasks[i].userValue = 0
            materialResiduesTasks[i].flag = true
        }
        materialResiduesRecyclerView.layoutManager = initLayoutManager()
        orderMaterialResiduesAdapter = OrderMaterialResiduesAdapter(materialResiduesTasks)
        materialRecyclerView.adapter = orderMaterialResiduesAdapter
    }

    private fun initActionBar() {
        title.text = R.string.task_details.toString()
    }

    @OnClick(R.id.arrow_back)
    internal fun arrowClick() = this.onBackPressed()

    @OnClick(R.id.order_send_report)
    internal fun clickReport() {
        if (orderMaterialAdapter.valueFlag() && orderMaterialResiduesAdapter.valueFlag) {
            //val sendTask = SendTask()
        }
    }

}