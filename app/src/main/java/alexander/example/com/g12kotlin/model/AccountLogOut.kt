package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 29.01.18.
 */
class AccountLogOut(
        @SerializedName("message") val message: String,
        @SerializedName("status") val status: Int)