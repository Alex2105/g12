package alexander.example.com.g12kotlin.activity

import alexander.example.com.g12kotlin.MainActivity
import alexander.example.com.g12kotlin.interfaces.SPLASH_DISPLAY_DURATION
import alexander.example.com.g12kotlin.interfaces.TOKEN
import alexander.example.com.g12kotlin.save.Preferences
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler


class SplashActivity : Activity() {
    lateinit var preferences: Preferences
    lateinit var context: Context


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this
        preferences = Preferences(context)
        Handler().postDelayed(Runnable {
            kotlin.run {
                val intent: Intent
                if (!preferences.loadToken(TOKEN).isEmpty()) {
                    intent = Intent(context, MainActivity::class.java)
                } else {
                    intent = Intent(context, AuthorizationActivity::class.java)
                }
                startActivity(intent)
                finish()
            }
        }, SPLASH_DISPLAY_DURATION)
    }
}