package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName


class SubtaskMaterial(@SerializedName("success") val success: Boolean,
                      @SerializedName("error") val error: String)