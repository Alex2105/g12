package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 31.01.18.
 */
class Gasoline(
        @SerializedName("name") val name: String,
        @SerializedName("value") val value: Int
)