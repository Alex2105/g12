package alexander.example.com.g12kotlin.fragment

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.activity.AuthorizationActivity
import alexander.example.com.g12kotlin.adapter.PersonAdapter
import alexander.example.com.g12kotlin.api.SingletonRest
import alexander.example.com.g12kotlin.utils.MaterialProgressDialog
import alexander.example.com.g12kotlin.errors.Error
import alexander.example.com.g12kotlin.interfaces.TOKEN
import alexander.example.com.g12kotlin.interfaces.USER_ID
import alexander.example.com.g12kotlin.model.AccountData
import alexander.example.com.g12kotlin.model.AccountLogOut
import alexander.example.com.g12kotlin.model.PersonData
import alexander.example.com.g12kotlin.save.Preferences
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by alexander on 29.01.18.
 */
class AccountFragment : Fragment() {
    @BindView(R.id.account_text)
    lateinit var accountText: TextView
    @BindView(R.id.log_out)
    lateinit var logOut: TextView
    @BindView(R.id.personal_data_recycler_view)
    lateinit var personRecyclerView: RecyclerView
    private lateinit var preferences: Preferences
    private lateinit var unbider: Unbinder
    private lateinit var token: String
    private lateinit var userData: ArrayList<String>
    private lateinit var materialProgressDialog: MaterialProgressDialog
    private lateinit var error: Error
    private var userId: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_account, container, false)
        val context = context
        error = Error(context)
        unbider = ButterKnife.bind(this, root)
        materialProgressDialog = MaterialProgressDialog.getProgressDialog(activity)
        preferences = Preferences(getContext())
        token = preferences.loadToken(TOKEN)
        userId = preferences.loadUserId(USER_ID)
        getAccountData(token, userId)
        return root
    }

    private fun initMaterialResiduesRecyclerView(userInformations: ArrayList<String>) {
        personRecyclerView.setHasFixedSize(true)
        personRecyclerView.layoutManager = LinearLayoutManager(context)
        val personInformations: List<PersonData> = personInformations(userData)
        val personAdapter = PersonAdapter(personInformations)
        personRecyclerView.adapter = personAdapter
        materialProgressDialog.dismiss()

    }

    @OnClick(R.id.log_out)
    fun setLogOut() {
        materialProgressDialog.show()
        SingletonRest.logOut(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setLogOut, { throwable -> error.handleError(throwable) })
    }

    private fun getAccountData(token: String, id: Int) {
        materialProgressDialog.show()
        SingletonRest.getAccountData(token, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setUserData, { throwable -> error.handleError(throwable) })
    }

    private fun setLogOut(accountLogOut: AccountLogOut) {
        if (accountLogOut.message.equals(getString(R.string.ok))) {
            preferences.removeToken(TOKEN)
            preferences.remove(USER_ID)
            val intent = Intent(context, AuthorizationActivity::class.java)
            startActivity(intent)
            materialProgressDialog.dismiss()
        }
    }

    private fun setUserData(accountData: AccountData) {
        userData = ArrayList()
        userData.add(accountData.firstName + " " + accountData.lastname)
        userData.add(accountData.phone)
        if (accountData.email != null) {
            userData.add(accountData.email)
        }
        initMaterialResiduesRecyclerView(userData)
    }

    private fun personInformations(userInformations: ArrayList<String>): List<PersonData> {
        val userData = ArrayList<PersonData>()
        userInformations.indices.mapTo(userData) { PersonData(userInformations[it]) }
        return userData
    }

    override fun onDestroy() {
        super.onDestroy()
        unbider.unbind()
    }
}