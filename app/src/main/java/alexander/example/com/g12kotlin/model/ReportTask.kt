package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 31.01.18.
 */
class ReportTask(
        @SerializedName("id") val id: Int,
        @SerializedName("title") val title: String,
        @SerializedName("address") val address: String,
        @SerializedName("description") val description: String,
        @SerializedName("date") val date: String,
        @SerializedName("materials") val materials: List<ReportMaterial>,
        @SerializedName("subtasks") val subtasks: List<ReportKindWork>,
        @SerializedName("comments") val comments: List<Comment>,
        @SerializedName("gasoline") val gasoline: Gasoline,
        @SerializedName("driver") val driverFullName: String,
        @SerializedName("users_materials") val reportResiduesMaterials: List<ReportResiduesMaterial>
)