package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.activity.OrderDetailsActivity
import alexander.example.com.g12kotlin.activity.ReportDetailsActivity
import alexander.example.com.g12kotlin.api.SingletonRest
import alexander.example.com.g12kotlin.model.*
import alexander.example.com.g12kotlin.save.Preferences
import android.content.Context
import alexander.example.com.g12kotlin.errors.Error
import alexander.example.com.g12kotlin.interfaces.KEY_MATERIALS_LIST
import alexander.example.com.g12kotlin.interfaces.KEY_REPORT_ID
import alexander.example.com.g12kotlin.interfaces.KEY_TASK_ID
import alexander.example.com.g12kotlin.interfaces.TOKEN
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class TaskAdapter(private var recyclerView: RecyclerView, private var tasks: List<Task>, private var context: Context) : RecyclerView.Adapter<TaskAdapter.ViewHolder>() {
    private lateinit var taskMaterials: TaskMaterials
    private var preferences = Preferences(context)
    private lateinit var materialTask: MaterialTask
    private var error: Error = Error(context)
    private var price: String = ""
    private var currency: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.task_item, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = tasks[position]
        holder.taskName.text = item.title
        if (!item.projectName.isEmpty()) {
            holder.projectName.visibility = View.VISIBLE
            holder.projectName.text = item.projectName
        } else {
            holder.projectName.visibility = View.GONE
        }
        holder.taskStreet.text = item.address
        if (item.status.equals("active")) {
            holder.firstRelativeLayout.background = context.resources.getDrawable(R.drawable.item_task_active_selector)
        } else {
            holder.firstRelativeLayout.background = context.resources.getDrawable(R.drawable.item_task_resolved_selector)
        }
        if (!tasks.isEmpty()) {
            holder.bind(item.materials, context)
            when (item.isSecondRelativeVisible) {
                true -> {
                    holder.secondRelativeLayout.visibility = View.VISIBLE
                }
                else -> {
                    holder.secondRelativeLayout.visibility = View.GONE
                }
            }

            holder.itemView.setOnClickListener {
                if (item.status.equals("active")) {
                    item.isSecondRelativeVisible = !item.isSecondRelativeVisible
                    notifyItemChanged(position, holder.secondRelativeLayout)
                    if (item.isSecondRelativeVisible)
                        this@TaskAdapter.recyclerView.smoothScrollToPosition(position)
                } else {
                    val intent = Intent(context, ReportDetailsActivity::class.java)
                    intent.putExtra(KEY_REPORT_ID, item.id)
                    context.startActivity(intent)
                }
            }
        }

        holder.detailsButton.setOnClickListener {
            val taskMaterial = ArrayList<SendTaskMaterial>()
            for (i in 0 until holder.materialAdapter.materials.size) {
                val sendTaskMaterial = SendTaskMaterial(holder.materialAdapter.materials[position].id,
                        holder.materialAdapter.materials[position].name,
                        holder.materialAdapter.materials[position].value,
                        holder.materialAdapter.materials[position].userIdPresent)
                taskMaterial.add(sendTaskMaterial)
            }
            var materialTask = MaterialTask(taskMaterial, item.id)
            val token: String = preferences.loadToken(TOKEN)
            SingletonRest.setMaterials(token, materialTask)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                val intent = Intent(context, OrderDetailsActivity::class.java)
                                intent.putExtra(KEY_TASK_ID, item.id)
                                intent.putExtra(KEY_MATERIALS_LIST, Gson().toJson(holder.materialAdapter.materials))
                                context.startActivity(intent)
                            },
                            { throwable -> error.handleError(throwable) })
        }
        holder.addMaterial.setOnClickListener {
            addNewMaterialDialog(item, holder)
        }

    }

    private fun setTextView(first: TextView, second: TextView, third: TextView) {
        first.setBackgroundResource(R.drawable.button_send_report_selector)
        first.setTextColor(context.resources.getColor(R.color.white))
        second.setBackgroundResource(R.drawable.frame_selector_blue)
        second.setTextColor(context.resources.getColor(R.color.black))
        third.setBackgroundResource(R.drawable.frame_selector_blue)
        third.setTextColor(context.resources.getColor(R.color.black))

    }

    private fun addNewMaterialDialog(item: Task, holder: ViewHolder) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val customView = inflater.inflate(R.layout.dialog_material_layout, null)
        val trade = customView.findViewById(R.id.trade) as TextView
        val retail = customView.findViewById(R.id.retail) as TextView
        val purchase = customView.findViewById(R.id.purchase) as TextView
        val materialName = customView.findViewById(R.id.material_name) as EditText
        val count = customView.findViewById(R.id.count) as EditText
        val cost = customView.findViewById(R.id.cost) as EditText
        val hryvnia = customView.findViewById(R.id.hryvnia_currency) as ImageView
        val dollar = customView.findViewById(R.id.dollar_currency) as ImageView
        trade.setOnClickListener {
            setTextView(trade, retail, purchase)
            price = context.resources.getString(R.string.trade)
        }
        retail.setOnClickListener {
            setTextView(retail, trade, purchase)
            price = context.resources.getString(R.string.retail)
        }
        purchase.setOnClickListener {
            setTextView(purchase, retail, trade)
            price = context.resources.getString(R.string.purchase)
        }
        hryvnia.setOnClickListener {
            currency = "UAH"
            hryvnia.setImageResource(R.drawable.hryvnia_select)
            dollar.setImageResource(R.drawable.dollar)
        }
        dollar.setOnClickListener {
            currency = "USD"
            dollar.setImageResource(R.drawable.dollar_select)
            hryvnia.setImageResource(R.drawable.hryvnia)
        }

        val materialDialog = MaterialStyledDialog.Builder(context)
                .withDialogAnimation(true)
                .setIcon(R.drawable.g12)
                .autoDismiss(false)
                .setHeaderColor(R.color.green)
                .setCustomView(customView, 10, 0, 10, 0)
                .setPositiveText("Ok")
                .setNegativeText("Cancel")
                .onNegative { dialog, which ->
                    dialog.dismiss()
                    currency = ""
                }
                .onNeutral { dialog, which ->
                    dialog.dismiss()
                    currency = ""
                }
                .onPositive { dialog, which ->
                    if (!price.isEmpty() && !count.text.toString().isEmpty() &&
                            !cost.text.toString().isEmpty() && !materialName.text.toString().isEmpty() && !currency.isEmpty()) {
                        val token = preferences.loadToken(TOKEN)
                        taskMaterials = TaskMaterials()
                        taskMaterials.materialsId = item.id
                        taskMaterials.materialsName = materialName.text.toString()
                        taskMaterials.materialsValue = count.text.toString().toInt()
                        taskMaterials.priceData(price, cost.text.toString().toDouble(), currency)
                        SingletonRest.setMaterial(token, taskMaterials)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({ taskAddMaterial ->
                                    addMaterial(taskAddMaterial, holder, token, item.id);
                                    dialog.dismiss()
                                }, { throwable ->
                                    error.handleError(throwable)
                                    dialog.dismiss()
                                })
                        price = ""
                        currency = ""


                    } else {
                        Toast.makeText(context, "Заполните все поля", Toast.LENGTH_SHORT).show()
                    }
                }.show()

    }

    private fun addMaterial(taskAddMaterial: TaskAddMaterial, holder: ViewHolder, token: String, taskId: Int) {
        SingletonRest.getUpdateMaterials(token, taskId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ materials ->
                    if (materials.materials != null) {
                        var materialList = ArrayList<TaskMaterial>()
                        for (i in 0 until materials.materials.size) {
                            val taskMaterial = TaskMaterial(materials.materials[i].id,
                                    materials.materials[i].name, materials.materials[i].value.toString(),
                                    materials.materials[i].isChecked)
                        }
                        holder.materialAdapter.setMaterials(materialList)

                    }
                }, { throwable -> error.handleError(throwable) })
    }

    override fun getItemCount(): Int = tasks.size

    fun updateTask(tasks: List<Task>) {
        this.tasks.toMutableList().clear()
        this.tasks = tasks
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.project_name)
        lateinit var projectName: TextView
        @BindView(R.id.text_name)
        lateinit var taskName: TextView
        @BindView(R.id.text_street)
        lateinit var taskStreet: TextView
        @BindView(R.id.first_relative_layout)
        lateinit var firstRelativeLayout: RelativeLayout
        @BindView(R.id.second_relative_layout)
        lateinit var secondRelativeLayout: RelativeLayout
        @BindView(R.id.add_new_material)
        lateinit var addMaterial: TextView
        @BindView(R.id.order_material_text)
        lateinit var materialText: TextView
        @BindView(R.id.order_details_button)
        lateinit var detailsButton: Button
        @BindView(R.id.order_material_recycler_view)
        lateinit var recyclerView: RecyclerView
        lateinit var materialAdapter: TaskMaterialAdapter

        init {
            ButterKnife.bind(this, itemView)
        }

        fun bind(materials: List<TaskMaterial>, context: Context) {
            val mLayoutManager = LinearLayoutManager(context)
            recyclerView.layoutManager = mLayoutManager
            materialAdapter = TaskMaterialAdapter(materials, context)
            recyclerView.adapter = materialAdapter
        }
    }

}