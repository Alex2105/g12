package alexander.example.com.g12kotlin.activity

import alexander.example.com.g12kotlin.MainActivity
import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.interfaces.KEY_ERROR
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick


class ErrorActivity : Activity() {
    @BindView(R.id.error)
    lateinit var errorText: TextView
    @BindView(R.id.retry_button)
    lateinit var retryButton: Button
    private lateinit var error: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_error)
        ButterKnife.bind(this)
        val intent = intent
        if (intent != null) {
            error = intent.getStringExtra(KEY_ERROR)
            errorText.text = error
        }

    }

    @OnClick(R.id.retry_button)
    internal fun clickRetryButton() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onBackPressed() = finishAffinity()

}