package alexander.example.com.g12kotlin.errors

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class InternetConnectionUtils {
    companion object {
        fun isInternetConnectionAvailable(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

        fun isInternetConnectionError(error: Throwable): Boolean =
                isInternetConnectionErrorInternal(error) ||
                        isInternetConnectionErrorInternal(error.cause!!)

        private fun isInternetConnectionErrorInternal(error: Throwable): Boolean =
                (error is UnknownHostException ||
                        error is ConnectException ||
                        error is SocketTimeoutException)
    }


}