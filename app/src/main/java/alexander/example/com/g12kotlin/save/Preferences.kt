package alexander.example.com.g12kotlin.save

import android.content.Context
import android.content.SharedPreferences


class Preferences(var context: Context) {
    lateinit var sharedPreferences: SharedPreferences

    fun save(udate: Boolean, constant: String) {
        sharedPreferences = context.getSharedPreferences(constant, Context.MODE_PRIVATE)
        var sharedPreferensEditor = sharedPreferences.edit()
        sharedPreferensEditor.putBoolean(constant, udate)
        sharedPreferensEditor.commit()
    }

    fun load(constant: String): Boolean {
        var updateState = false
        sharedPreferences = context.getSharedPreferences(constant, Context.MODE_PRIVATE)
        if (sharedPreferences.contains(constant)) {
            updateState = sharedPreferences.getBoolean(constant, false)
        }
        return updateState
    }

    fun remove(constant: String) {
        sharedPreferences = context.getSharedPreferences(constant, Context.MODE_PRIVATE)
        sharedPreferences.edit().clear().commit()
    }

    fun saveToken(token: String, constant: String) {
        sharedPreferences = context.getSharedPreferences(constant, Context.MODE_PRIVATE)
        var sharedPreferensEditor = sharedPreferences.edit()
        sharedPreferensEditor.putString(constant, token)
        sharedPreferensEditor.commit()
    }

    fun loadToken(constant: String): String {
        var token = ""
        sharedPreferences = context.getSharedPreferences(constant, Context.MODE_PRIVATE)
        if (sharedPreferences.contains(constant)) {
            token = sharedPreferences.getString(constant, "")
        }
        return token
    }

    fun removeToken(constant: String) {
        sharedPreferences = context.getSharedPreferences(constant, Context.MODE_PRIVATE)
        sharedPreferences.edit().clear().commit()
    }

    fun saveUserId(id: Int, constant: String) {
        sharedPreferences = context.getSharedPreferences(constant, Context.MODE_PRIVATE)
        var sharedPreferensEditor = sharedPreferences.edit()
        sharedPreferensEditor.putInt(constant, id)
        sharedPreferensEditor.commit()
    }

    fun loadUserId(constant: String): Int {
        var userId = 0
        sharedPreferences = context.getSharedPreferences(constant, Context.MODE_PRIVATE)
        if (sharedPreferences.contains(constant)) {
            userId = sharedPreferences.getInt(constant, 0)
        }

        return userId
    }

    fun removeIserId(constant: String) {
        sharedPreferences = context.getSharedPreferences(constant, Context.MODE_PRIVATE)
        sharedPreferences.edit().clear().commit()
    }

}