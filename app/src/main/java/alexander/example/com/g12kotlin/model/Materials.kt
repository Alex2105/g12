package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName


class Materials(@SerializedName("materials") val materials: List<Material>?)