package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 29.01.18.
 */
class Task(
        var isSecondRelativeVisible: Boolean = false,
        @SerializedName("project_name") val projectName: String,
        @SerializedName("id") val id: Int,
        @SerializedName("title") val title: String,
        @SerializedName("status") val status: String,
        @SerializedName("address") val address: String,
        @SerializedName("description") val descriptor: String,
        @SerializedName("date") val date: String,
        @SerializedName("materials") val materials: List<TaskMaterial>

)
