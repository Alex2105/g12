package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.model.PersonData
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife

class PersonAdapter(private val materialsResidues: List<PersonData>) : RecyclerView.Adapter<PersonAdapter.ViewHolder>() {
    override fun getItemCount(): Int = materialsResidues.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = materialsResidues[position].data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.person_data_item, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.personal_name)
        lateinit var name: TextView

        init {
            ButterKnife.bind(this, itemView)
        }

    }
}