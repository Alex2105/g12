package alexander.example.com.g12kotlin.interfaces


const val REPORT = "REPORT"
const val USER_ID = "USERID"
const val TOKEN = "TOKEN";
const val KEY_MATERIALS_LIST = "MATERIALSLIST"
const val KEY_TASK_ID = "TASKID"
const val KEY_REPORT_ID = "REPORTID"
const val KEY_SEARCH = "SEARCH"
const val KEY_ERROR = "ERRORMESSAGER"
const val KEY_UPDATE_LIST = "UPDATELIST"
const val UPDATE_FILES = " UPDATEFILES"
const val SPLASH_DISPLAY_DURATION: Long = 2000




