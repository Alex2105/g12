package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName


class Price {
    @SerializedName("point_input_price")
    var purchasePrice: Double = 0.0
    @SerializedName("point_opt_price")
    var tradePrice: Double = 0.0
    @SerializedName("point_retail_price")
    var retailPrice: Double = 0.0
    @SerializedName("input_currency")
    lateinit var purchaseCurrency: String
    @SerializedName("opt_currency")
    lateinit var tradeCurrency: String
    @SerializedName("retail_currency")
    lateinit var retailCurrency: String
}