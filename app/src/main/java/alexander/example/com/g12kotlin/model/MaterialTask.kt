package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 30.01.18.
 */
class MaterialTask(
        @SerializedName("materials") val materialList: List<SendTaskMaterial>,
        @SerializedName("task_id") val taskId: Int
)