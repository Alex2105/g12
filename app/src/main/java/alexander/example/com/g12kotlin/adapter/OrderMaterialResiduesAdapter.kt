package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.model.OrderResiduesMaterial
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife


class OrderMaterialResiduesAdapter(private var materialsResidues: List<OrderResiduesMaterial>) : RecyclerView.Adapter<OrderMaterialResiduesAdapter.ViewHolder>() {
    var valueFlag: Boolean = false
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.order_material_residues_item, parent, false)
        return ViewHolder(view)
    }

    fun valueFlag(): Boolean {
        valueFlag = true
        valueFlag = (0 until materialsResidues.size).any { materialsResidues[it].flag }
        return valueFlag
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: OrderResiduesMaterial = materialsResidues[position]
        holder.materialResiduesName.text = item.name
        holder.materialResiduesCount.setText(item.userValue.toString())
        holder.yourMaterialResiduesCount.text = item.value.toString()
        holder.materialResiduesCount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun afterTextChanged(editable: Editable?) {
                if (holder.materialResiduesCount.text.toString().isEmpty()) {
                    item.flag = false
                    holder.materialResiduesCount.setBackgroundColor(R.drawable.frame_selector_red)
                } else {
                    val materialCount = holder.materialResiduesCount.text.toString().toInt()
                    if (materialCount > item.value) {
                        item.flag = false
                        holder.materialResiduesCount.setBackgroundColor(R.drawable.frame_selector_red)
                    } else {
                        item.flag = true
                        holder.materialResiduesCount.setBackgroundResource(R.drawable.frame_selector_blue)
                    }

                }
                item.userValue = editable.toString().toInt()
            }

        })

    }

    override fun getItemCount(): Int = materialsResidues.size


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.order_material_residues_name)
        lateinit var materialResiduesName: TextView
        @BindView(R.id.order_material_residues_edit_text)
        lateinit var materialResiduesCount: EditText
        @BindView(R.id.order_your_material_residues_edit_text)
        lateinit var yourMaterialResiduesCount: TextView

        init {
            ButterKnife.bind(this, itemView)
        }

    }
}