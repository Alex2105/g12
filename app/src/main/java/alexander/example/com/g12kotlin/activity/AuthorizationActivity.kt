package alexander.example.com.g12kotlin.activity

import alexander.example.com.g12kotlin.MainActivity
import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.api.SingletonRest
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.preference.Preference
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import alexander.example.com.g12kotlin.errors.Error
import alexander.example.com.g12kotlin.interfaces.TOKEN
import alexander.example.com.g12kotlin.interfaces.USER_ID
import alexander.example.com.g12kotlin.model.Authorization
import alexander.example.com.g12kotlin.save.Preferences
import android.content.Intent
import android.util.Log
import android.view.KeyEvent
import butterknife.OnClick
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class AuthorizationActivity : Activity() {
    @BindView(R.id.phone_number)
    lateinit var phoneNumber: EditText
    @BindView(R.id.password)
    lateinit var password: EditText
    @BindView(R.id.authorization_button)
    lateinit var authorization: Button
    @BindView(R.id.sign_in_text)
    lateinit var signInText: TextView
    private lateinit var context: Context
    private lateinit var preferences: Preferences
    private lateinit var error: Error


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authorization)
        ButterKnife.bind(this)
        context = this
        preferences = Preferences(context)
        error = Error(context)
    }

    @OnClick(R.id.authorization_button)
    internal fun authorizationButtonClick() {
        SingletonRest.authorization(phoneNumber.text.toString(), password.text.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setAuthorization, { throwable -> error.handleErrorAuthorization(throwable, phoneNumber, password) })
    }

    private fun setAuthorization(authorization: Authorization) {
        if (authorization.success) {
            preferences.saveToken(authorization.token!!, TOKEN)
            preferences.saveUserId(authorization.id, USER_ID)
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true)
            return true
        }
        return false
    }


}