package alexander.example.com.g12kotlin.calendar

import android.text.format.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class CalendarData {
    companion object {
        fun getCurrentYear(): Int {
            val calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault())
            calendar.time = Date()
            return calendar.get(Calendar.YEAR)
        }

        fun dateFormat(date: Date): String {
            val simpleDateFormat = SimpleDateFormat("EEEE, dd MMMM, yyyy")
            return simpleDateFormat.format(date)
        }

        @Throws(ParseException::class)
        fun dateFormatFromServer(sdate: String): String {
            //var date:Date Раскометировать если будут ошибки
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
            val date = simpleDateFormat.parse(sdate)
            return DateFormat.format("EEEE, d MMMM, yyyy", date.time).toString()
        }

        @Throws(ParseException::class)
        fun dateFormatServer(sdate: String): String {
            //var date:Date Раскометировать если будут ошибки
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
            val date = simpleDateFormat.parse(sdate)
            return DateFormat.format("dd-MM-yyyy", date.time).toString()
        }

        fun dateFormatFromServer(date: Date): String {
            val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
            return simpleDateFormat.format(date)
        }

        fun minDate(): Calendar {
            val calendar = Calendar.getInstance()
            calendar.set(2017, 0, Calendar.DAY_OF_WEEK)
            return calendar
        }
    }
}