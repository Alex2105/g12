package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName


class Driver(
        @SerializedName("id") val id: Int,
        @SerializedName("firstname") val firstname: String,
        @SerializedName("lastname") val lastname: String

)