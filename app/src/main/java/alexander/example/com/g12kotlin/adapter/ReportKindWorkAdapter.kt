package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.model.ReportKindWork
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife


class ReportKindWorkAdapter(private var workList: List<ReportKindWork>) : RecyclerView.Adapter<ReportKindWorkAdapter.ViewHolder>() {
    override fun getItemCount(): Int = workList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: ReportKindWork = workList[position]
        holder.taskName.text = item.name
        if (item.created) {
            holder.stateTask.setImageResource(R.drawable.completed_task)
        } else {
            holder.stateTask.setImageResource(R.drawable.unfulfilled_task)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.report_kind_of_work_item, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.report_work)
        lateinit var taskName: TextView
        @BindView(R.id.task_state)
        lateinit var stateTask: ImageView

        init {
            ButterKnife.bind(this, itemView)
        }

    }
}