package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 31.01.18.
 */
class User(@SerializedName("id") val id: Int,
           @SerializedName("firstname") val firstname: String,
           @SerializedName("lastname") val lastname: String)