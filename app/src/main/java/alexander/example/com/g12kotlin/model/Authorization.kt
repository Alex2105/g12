package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName


class Authorization(
        @SerializedName("success") val success: Boolean,
        @SerializedName("token") val token: String?,
        @SerializedName("user_id") val id: Int
)