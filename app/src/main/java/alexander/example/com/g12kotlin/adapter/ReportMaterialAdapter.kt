package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.model.ReportMaterial
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife


class ReportMaterialAdapter(private var materials: List<ReportMaterial>) : RecyclerView.Adapter<ReportMaterialAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.report_material_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: ReportMaterial = materials[position]
        holder.materialName.text = item.name
        holder.materialCount.text = item.value.toString()

    }

    override fun getItemCount(): Int = materials.size


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.report_material_name)
        lateinit var materialName: TextView
        @BindView(R.id.report_material_text)
        lateinit var materialCount: TextView

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}