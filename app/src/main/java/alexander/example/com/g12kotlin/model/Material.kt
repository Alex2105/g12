package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 31.01.18.
 */
class Material {
    @SerializedName("id")
    val id: Int = 0
    @SerializedName("name")
    val name: String = ""
    @SerializedName("date")
    val date: String = ""
    @SerializedName("value")
    val value: Int = 0
    @SerializedName("user_id_present")
    val isChecked: Boolean = false
    @SerializedName("for_transaction")
    val flagCheckbox: Boolean = false
    val flag: Boolean = false
    val maxValue: Int = 0
}