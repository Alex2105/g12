package alexander.example.com.g12kotlin

import alexander.example.com.g12kotlin.adapter.ViewPageAdapter
import alexander.example.com.g12kotlin.fragment.AccountFragment
import alexander.example.com.g12kotlin.fragment.ActiveFragment
import alexander.example.com.g12kotlin.fragment.MaterialsFragment
import alexander.example.com.g12kotlin.fragment.StorageFragment
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import net.danlew.android.joda.JodaTimeAndroid
import java.util.jar.Manifest

class MainActivity : AppCompatActivity() {
    @BindView(R.id.tablayout)
    lateinit var tablayout: TabLayout
    @BindView(R.id.viewpager)
    lateinit var viewpager: ViewPager
    private val images: IntArray = intArrayOf(R.drawable.active_selector, R.drawable.materials_selector, R.drawable.storage_selector, R.drawable.account_selector)
    private val tabItemsName = arrayOf("Задачи", "Материалы", "Хранилище", "Акаунт")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        JodaTimeAndroid.init(this)
        ButterKnife.bind(this)
        setupViewPage(viewpager)
        viewpager.offscreenPageLimit = 1
        tablayout.setupWithViewPager(viewpager)
        setupTabIcons()
        isStoragePermissionGranted()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true)
            return true
        }
        return false
    }

    private fun setupViewPage(viewPager: ViewPager) {
        val adapter = ViewPageAdapter(supportFragmentManager)
        adapter.addFragment(ActiveFragment(), tabItemsName[0])
        adapter.addFragment(MaterialsFragment(), tabItemsName[1])
        adapter.addFragment(StorageFragment(), tabItemsName[2])
        adapter.addFragment(AccountFragment(), tabItemsName[3])
        viewPager.adapter = adapter
    }

    private fun setupTabIcons() {
        for (i in 0 until tablayout.tabCount) {
            val view = LayoutInflater.from(this).inflate(R.layout.tab_item, null)
            val textItem = view.findViewById(R.id.tab_text) as TextView
            val imageItem = view.findViewById(R.id.tab_image) as ImageView
            textItem.text = tabItemsName[i]
            imageItem.setImageResource(images[i])
            if (i == 0) {
                view.isSelected = true
            }
            tablayout.getTabAt(i)!!.customView = view

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v("XXX", "Permission" + permissions[0] + "was" + grantResults[0])
        } else {
            Toast.makeText(this, "Application does not correct", Toast.LENGTH_LONG).show()
        }
    }

    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v("XXX", "Permission is granted")
                return true
            } else {
                Log.v("XXX", "Permission is revoked")
                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                return false
            }
        } else {
            Log.v("XXX", "Permission is granted")
            return true
        }
    }
}
