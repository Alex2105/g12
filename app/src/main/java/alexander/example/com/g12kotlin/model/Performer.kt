package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 02.02.18.
 */
class Performer(
        @SerializedName("firstname") val firstName: String,
        @SerializedName("lastname") val lastname: String,
        @SerializedName("id") val id: Int
)