package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName


class TaskAddMaterial(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("date") val date: String,
        @SerializedName("value") val value: Int,
        @SerializedName("user_id_present") val userIdPresent: Boolean,
        @SerializedName("for_transaction") val forTransaction: Boolean
)