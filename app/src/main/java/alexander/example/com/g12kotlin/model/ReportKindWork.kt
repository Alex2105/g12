package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 31.01.18.
 */
class ReportKindWork(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("created?") val created: Boolean
)