package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.model.OrderMaterial
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife


class OrderMaterialAdapter(private var materials: List<OrderMaterial>, private var valueFlag: Boolean) : RecyclerView.Adapter<OrderMaterialAdapter.ViewHolder>() {
    override fun getItemCount(): Int = materials.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: OrderMaterial = materials[position]
        holder.materialName.text = item.name
        holder.materialCount.setText(item.value.toString())
        holder.materialCount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun afterTextChanged(editable: Editable?) =
                    if (holder.materialCount.text.toString().isEmpty()) {
                        item.flag = false
                        holder.materialCount.setBackgroundResource(R.drawable.frame_selector_red)
                    } else {
                        val materialCount = holder.materialCount.text.toString().toInt()
                        if (materialCount > item.maxValue) {
                            item.flag = true
                            valueFlag = false
                            holder.materialCount.setBackgroundResource(R.drawable.frame_selector_red)
                        } else {
                            item.flag = true
                            holder.materialCount.setBackgroundColor(R.drawable.frame_selector_blue)
                        }
                        item.value = editable.toString().toInt()


                    }
        })
    }

    fun valueFlag(): Boolean {
        valueFlag = true
        for (i in 0 until materials.size) {
            if (!materials[i].flag) {
                valueFlag = false
                break
            }
        }
        return valueFlag
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.oreder_material_item, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.order_material_name)
        lateinit var materialName: TextView
        @BindView(R.id.order_material_edit_text)
        lateinit var materialCount: EditText

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}