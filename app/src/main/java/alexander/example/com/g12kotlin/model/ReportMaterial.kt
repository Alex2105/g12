package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName


class ReportMaterial(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("value") val value: Int,
        @SerializedName("user_id_present") val userIdPresent: Boolean
)