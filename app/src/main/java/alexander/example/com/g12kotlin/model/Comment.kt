package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 31.01.18.
 */
class Comment(
        @SerializedName("id") val id: Int,
        @SerializedName("task_id") val taskId: Int,
        @SerializedName("description") val description: String,
        @SerializedName("created_at") val createdAt: String,
        @SerializedName("user") val user: User
)