package alexander.example.com.g12kotlin.fragment

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.adapter.TaskAdapter
import alexander.example.com.g12kotlin.api.SingletonRest
import alexander.example.com.g12kotlin.calendar.CalendarData
import alexander.example.com.g12kotlin.calendar.HorizontalCalendar
import alexander.example.com.g12kotlin.calendar.HorizontalCalendarListener
import alexander.example.com.g12kotlin.calendar.HorizontalCalendarView
import alexander.example.com.g12kotlin.save.Preferences
import alexander.example.com.g12kotlin.utils.MaterialProgressDialog
import alexander.example.com.g12kotlin.errors.Error
import alexander.example.com.g12kotlin.interfaces.KEY_SEARCH
import alexander.example.com.g12kotlin.interfaces.REPORT
import alexander.example.com.g12kotlin.interfaces.TOKEN
import alexander.example.com.g12kotlin.model.Task
import alexander.example.com.g12kotlin.model.Tasks
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.joda.time.DateTime
import java.util.*


class ActiveFragment : Fragment(), DatePickerDialog.OnDateSetListener, TextView.OnEditorActionListener {
    @BindView(R.id.recycler_view)
    lateinit var recyclerView: RecyclerView
    @BindView(R.id.search_edit_text)
    lateinit var search: EditText
    @BindView(R.id.text_my_task)
    lateinit var myTaskText: TextView
    @BindView(R.id.first_relative_layout)
    lateinit var relativeLayout: RelativeLayout
    @BindView(R.id.day_number)
    @JvmField
    var day: TextView? = null
    @BindView(R.id.swipeRefreshLayout_active)
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var horizontalCalendar: HorizontalCalendar
    private lateinit var unbinder: Unbinder
    private lateinit var taskAdapter: TaskAdapter
    private lateinit var preferences: Preferences
    private lateinit var date: Date
    private lateinit var dateTimeCalendar: DateTime
    private lateinit var dateTimeHorizontalCalendar: DateTime
    private lateinit var datePickerDialog: DatePickerDialog
    private var horizontalCalendarFlag: Boolean = false
    private var calendarFlag: Boolean = false
    private lateinit var token: String
    private lateinit var swipeDate: String
    private lateinit var updateDate: String
    private lateinit var materialProgressDialog: MaterialProgressDialog
    private lateinit var error: Error


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_active, container, false)
        unbinder = ButterKnife.bind(this, root)
        error = Error(context)
        search.setOnEditorActionListener(this)
        preferences = Preferences(context)
        token = preferences.loadToken(TOKEN)
        materialProgressDialog = MaterialProgressDialog.getProgressDialog(activity)
        addCalendar(root)
        setDate()
        swipe()
        getTasks(CalendarData.dateFormatFromServer(date))
        return root
    }

    private fun swipe() {
        swipeRefreshLayout.setColorSchemeColors(R.color.blue)
        swipeRefreshLayout.setOnRefreshListener {
            swipeDate = when {
                horizontalCalendarFlag -> CalendarData.dateFormatFromServer(dateTimeHorizontalCalendar.toDate())
                calendarFlag -> CalendarData.dateFormatFromServer(dateTimeCalendar.toDate())
                else -> CalendarData.dateFormatFromServer(date)
            }
            updateDate(swipeDate)
        }
    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        calendarFlag(false, true)
        dateTimeCalendar = DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0, 0)
        horizontalCalendar.selectDate(dateTimeCalendar.toDate(), true)
        day!!.text = CalendarData.dateFormat(dateTimeCalendar.toDate())
        getUpdateTast(CalendarData.dateFormatFromServer(dateTimeCalendar.toDate()))
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            if (!search.text.toString().isEmpty()) {
//                val intent = Intent(activity, SearchActivity::class.java)
//                intent.putExtra(KEY_SEARCH, search.text.toString())
//                startActivity(intent)
                search.setText("")
            }
            return false
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        if (preferences.load(REPORT)) {
            materialProgressDialog.show()
            updateDate(updateDate)
        }
    }

    private fun updateDate(sUpdateDate: String) {
        var updateDate = sUpdateDate
        updateDate = when {
            horizontalCalendarFlag -> CalendarData.dateFormatFromServer(dateTimeHorizontalCalendar.toDate())
            calendarFlag -> CalendarData.dateFormatFromServer(dateTimeCalendar.toDate())
            else -> CalendarData.dateFormatFromServer(date)
        }
        SingletonRest.tasks(token, sUpdateDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::updateList, { throwable -> error.handleError(throwable) })
    }

    private fun setDate() {
        date = Date()
        day!!.text = CalendarData.dateFormat(date)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            if (day != null) {
                date = Date()
                materialProgressDialog.dismiss()
                horizontalCalendar.selectDate(date, true)
                calendarFlag(false, false)
                setDate()
                getTasks(CalendarData.dateFormatFromServer(date))
            } else {

                calendarFlag(false, false)
            }
        }

    }

    @OnClick(R.id.day_number)
    internal fun calendarClick() {
        val calendar = Calendar.getInstance()
        when {
            horizontalCalendarFlag -> calendarDate(dateTimeHorizontalCalendar)
            calendarFlag -> calendarDate(dateTimeCalendar)
            else -> calendarDate(calendar)
        }
        datePickerDialog.minDate = CalendarData.minDate()
        val year = CalendarData.getCurrentYear()
        datePickerDialog.setYearRange(year - 1, year)
        calendar.add(Calendar.MONTH, 12)
        datePickerDialog.maxDate = calendar
        datePickerDialog.accentColor = resources.getColor(R.color.blue)
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_1)
        datePickerDialog.dismissOnPause(true)
        datePickerDialog.show(activity.fragmentManager, "Datepickerdialog")
    }

    private fun calendarDate(dateTime: DateTime) {
        datePickerDialog = DatePickerDialog.newInstance(
                this@ActiveFragment,
                dateTime.year,
                dateTime.monthOfYear - 1,
                dateTime.dayOfMonth)
    }

    private fun calendarDate(calendar: Calendar) {
        datePickerDialog = DatePickerDialog.newInstance(
                this@ActiveFragment,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        )
    }


    private fun updateList(tasks: Tasks) {
        taskAdapter.updateTask(tasks.tasks)
        preferences.remove(REPORT)
        materialProgressDialog.dismiss()
        swipeRefreshLayout.isRefreshing = false
    }

    private fun initRecyclerView(tasks: List<Task>) {
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(context)
        taskAdapter = TaskAdapter(recyclerView, tasks, context)
        recyclerView.adapter = taskAdapter
        materialProgressDialog.dismiss()
    }

    private fun getTasks(data: String) {
        materialProgressDialog.show()
        SingletonRest.tasks(token, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::addTasks, { throwable -> error.handleError(throwable) })
    }

    private fun getUpdateTast(data: String) {
        materialProgressDialog.show()
        SingletonRest.tasks(token, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::updateList, { throwable -> error.handleError(throwable) })
    }

    private fun addTasks(tasks: Tasks) {
        materialProgressDialog.dismiss()
        initRecyclerView(tasks.tasks)
    }

    private fun addCalendar(root: View) {
        val endDate = Calendar.getInstance()
        endDate.add(Calendar.MONTH, 12)
        val startDate = Calendar.getInstance()
        startDate.add(Calendar.MONTH, -10)
        horizontalCalendar = HorizontalCalendar.Builder(root, R.id.calendar)
                .startDate(startDate.time)
                .endDate(endDate.time)
                .build()
        horizontalCalendar.calendarListener = object : HorizontalCalendarListener() {
            override fun onDateSelected(date: Date, position: Int) {
                calendarFlag(true, false)
                dateTimeHorizontalCalendar = DateTime(date)
                day!!.text = CalendarData.dateFormat(date)
                getUpdateTast(CalendarData.dateFormatFromServer(date))
            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unbinder.unbind()
    }

    private fun calendarFlag(hFlag: Boolean, cFlag: Boolean) {
        horizontalCalendarFlag = hFlag
        calendarFlag = cFlag


    }


}