package alexander.example.com.g12kotlin.errors

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.activity.AuthorizationActivity
import alexander.example.com.g12kotlin.activity.ErrorActivity
import alexander.example.com.g12kotlin.interfaces.KEY_ERROR
import alexander.example.com.g12kotlin.interfaces.TOKEN
import android.content.Context
import retrofit2.HttpException
import alexander.example.com.g12kotlin.save.Preferences
import android.content.Intent
import android.widget.EditText
import android.widget.Toast


class Error {
    private lateinit var exception: Exception
    private lateinit var message: String
    private lateinit var httpException: HttpException
    private var context: Context
    private var flag = false
    private var preferences: Preferences

    constructor(context: Context) {
        this.context = context
        preferences = Preferences(context)

    }

    fun handleErrorAuthorization(throwable: Throwable, phoneNumber: EditText, password: EditText) {
        exception = throwable as Exception
        if (exception is HttpException) {
            httpException = exception as HttpException
            when (httpException.code()) {
                401 -> {
                    flag = false
                    phoneNumber.setText("")
                    password.setText("")
                    Toast.makeText(context, R.string.wrong_login_or_password, Toast.LENGTH_SHORT).show();
                }
                else -> {
                    flag = true
                    message = context.getString(R.string.server_error)
                }

            }

        } else if (InternetConnectionUtils.isInternetConnectionError(exception)) {
            if (!InternetConnectionUtils.isInternetConnectionAvailable(context)) {
                flag = true
                message = context.getString(R.string.no_internet_connection)
            } else {
                flag = true
                message = context.getString(R.string.internet_connection_error)
            }
        }
        if (flag) {
            errorActivity(message)
        }
    }

    fun handleError(throwable: Throwable) {
        exception = throwable as Exception
        if (exception is HttpException) {
            httpException = exception as HttpException
            when (httpException.code()) {
                401 -> {
                    flag = false
                    errorUnauthorized()
                }
                else -> {
                    flag = true
                    message = context.getString(R.string.server_error)
                }
            }
        } else if (InternetConnectionUtils.isInternetConnectionError(exception)) {
            if (!InternetConnectionUtils.isInternetConnectionAvailable(context)) {
                flag = true
                message = context.getString(R.string.no_internet_connection)
            } else {
                flag = true
                message = context.getString(R.string.internet_connection_error)
            }
        }
        if (flag) {
            errorActivity(message)
        }

    }

    fun errorActivity(message: String) {
        val intent = Intent(context, ErrorActivity::class.java)
        intent.putExtra(KEY_ERROR, message)
        context.startActivity(intent)
    }

    fun errorUnauthorized() {
        val intent = Intent(context, AuthorizationActivity::class.java)
        context.startActivity(intent)
        preferences.removeToken(TOKEN)
    }
}