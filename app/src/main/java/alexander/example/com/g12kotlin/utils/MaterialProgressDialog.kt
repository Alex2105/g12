package alexander.example.com.g12kotlin.utils

import alexander.example.com.g12kotlin.R
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar

/**
 * Created by alexander on 29.01.18.
 */
class MaterialProgressDialog(context: Context?) : Dialog(context, R.style.ProgressDialogTheme) {
    companion object {
        fun getProgressDialog(activity: Activity): MaterialProgressDialog {
            val linearLayout = LinearLayout(activity)
            linearLayout.orientation = LinearLayout.VERTICAL
            val progressBar = ProgressBar(activity)
            progressBar.indeterminateDrawable = activity.resources.getDrawable(R.drawable.progress_bar)
            linearLayout.addView(progressBar)
            val dialog = MaterialProgressDialog(activity)
            dialog.setCancelable(false)
            dialog.ownerActivity = activity
            dialog.addContentView(linearLayout, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
            return dialog
        }
    }
}