package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName


class TaskMaterial(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("value") val value: String,
        @SerializedName("user_id_present?") var userIdPresent: Boolean
)