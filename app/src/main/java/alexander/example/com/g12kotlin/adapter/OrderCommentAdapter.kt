package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.model.Comment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife


class OrderCommentAdapter(private var comments: List<Comment>) : RecyclerView.Adapter<OrderCommentAdapter.ViewHolder>() {
    override fun getItemCount(): Int = comments.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.comment.text = comments[position].description
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.order_comment_item, parent, false)
        return ViewHolder(view)
    }

    fun setUpdateComments(comments: List<Comment>) {
        this.comments.toMutableList().clear()
        this.comments = comments
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.order_comment_item_text)
        lateinit var comment: TextView

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}