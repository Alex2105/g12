package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 01.02.18.
 */
class OrderMaterial(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("value") var value: Int,
        @SerializedName("user_id_present?") val userIdPresent: Boolean,
        var flag: Boolean,
        var maxValue: Int
)