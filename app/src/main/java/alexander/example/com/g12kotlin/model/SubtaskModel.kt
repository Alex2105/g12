package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 01.02.18.
 */
class SubtaskModel(
        @SerializedName("success") val success: Boolean
)