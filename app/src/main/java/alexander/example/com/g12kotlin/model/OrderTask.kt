package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName

/**
 * Created by alexander on 02.02.18.
 */
class OrderTask(
        @SerializedName("id") val id: Int,
        @SerializedName("title") val title: String,
        @SerializedName("address") val address: String,
        @SerializedName("description") val description: String,
        @SerializedName("date") val date: String,
        @SerializedName("project_name") val projectName: String,
        @SerializedName("boss_id") val brigadierId: Int,
        @SerializedName("materials") val materials: List<OrderMaterial>,
        @SerializedName("subtasks") val subtasks: List<OrderKindWork>,
        @SerializedName("comments") val comments: List<Comment>,
        @SerializedName("users_with_car") val driverList: List<Driver>,
        @SerializedName("users_materials") val usersMaterials: List<OrderResiduesMaterial>,
        @SerializedName("users") val performerList: List<Performer>
)