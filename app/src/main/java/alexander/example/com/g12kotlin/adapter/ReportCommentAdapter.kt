package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import android.support.v7.widget.RecyclerView
import alexander.example.com.g12kotlin.model.Comment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife


class ReportCommentAdapter(private var comments: List<Comment>) : RecyclerView.Adapter<ReportCommentAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.comment.text = comments[position].description
    }

    override fun getItemCount(): Int = comments.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportCommentAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.report_comment_item, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.report_comment_item_text)
        lateinit var comment: TextView

        init {
            ButterKnife.bind(this, itemView)
        }

    }

}