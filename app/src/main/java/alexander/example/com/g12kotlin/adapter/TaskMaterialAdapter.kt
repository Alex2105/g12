package alexander.example.com.g12kotlin.adapter

import alexander.example.com.g12kotlin.R
import alexander.example.com.g12kotlin.model.TaskMaterial
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SwitchCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife


class TaskMaterialAdapter(var materials: List<TaskMaterial>, var context: Context) : RecyclerView.Adapter<TaskMaterialAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = materials[position]
        holder.materialName.text = item.name + " " + item.value
        holder.checked.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            run {
                item.userIdPresent = b
            }
        })
    }

    fun setMaterials(materials: ArrayList<TaskMaterial>) {
        this.materials = materials
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskMaterialAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.task_material_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = materials.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.material_name)
        lateinit var materialName: TextView
        @BindView(R.id.material_switch)
        lateinit var checked: SwitchCompat

        init {
            ButterKnife.bind(this, itemView)
        }

    }


}