package alexander.example.com.g12kotlin.api

import alexander.example.com.g12kotlin.model.*
import io.reactivex.Observable
import retrofit2.http.*


class ServerRequest {
    interface IAuthorization {
        @POST("auth/login")
        fun setAuthorization(@Query("phone") phoneNumber: String, @Query("password") password: String): Observable<Authorization>
    }

    interface ILogOut {
        @DELETE("auth/logout")
        fun setLogOut(@Header("Access-Token") token: String): Observable<AccountLogOut>
    }

    interface IAccountData {
        @GET("users/{id}")
        fun setAccountData(@Header("Access-Token") token: String, @Path("id") id: Int): Observable<AccountData>
    }

    interface ITaskMaterial {
        @POST("materials/to_user")
        fun setMaterials(@Header("Access-Token") token: String, @Body materialTask: MaterialTask): Observable<SubtaskMaterial>
    }

    interface IUpdateTaskMaterials {
        @GET("materials")
        fun getMaterialsList(@Header("Access-Token") token: String, @Query("task_id") taskId: Int): Observable<Materials>
    }

    interface IAddMaterial {
        @POST("materials")
        fun setMaterial(@Header("Access-Token") token: String, @Body taskMaterials: TaskMaterials): Observable<TaskAddMaterial>
    }

    interface IReportTask {
        @GET("tasks/{id}")
        fun getReportsData(@Header("Access-Token") token: String, @Path("id") id: Int): Observable<ReportTask>
    }

    interface ISubtask {
        @PUT("subtasks/{id}")
        fun sendSubtask(@Header("Access-Token") token: String, @Path("id") subtaskId: Int, @Query("created?") subtaskState: Boolean): Observable<SubtaskModel>
    }

    interface ITasks {
        @GET("tasks")
        fun getTasks(@Header("Access-Token") token: String, @Query("tasks[date]") data: String): Observable<Tasks>
    }

    interface ITaskData {
        @GET("tasks/{id}")
        fun getOrdersData(@Header("Access-Token") token: String, @Path("id") id: Int): Observable<OrderTask>
    }
}