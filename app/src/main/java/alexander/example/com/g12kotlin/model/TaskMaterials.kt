package alexander.example.com.g12kotlin.model

import com.google.gson.annotations.SerializedName


class TaskMaterials {
    @SerializedName("name")
    lateinit var materialsName: String
    @SerializedName("value")
    var materialsValue: Int = 0
    @SerializedName("task_id")
    var materialsId: Int = 0

    @SerializedName("price_attributes") lateinit var price: Price

    fun priceData(name: String, priceCount: Double, currency: String) {
        price = Price()
        when (name) {
            "Оптовая" -> {
                price.tradePrice = priceCount
                price.tradeCurrency = currency
            }
            "Розничная" -> {
                price.retailPrice = priceCount
                price.retailCurrency = currency
            }
            else -> {
                price.purchasePrice = priceCount
                price.purchaseCurrency = currency
            }
        }
    }
}